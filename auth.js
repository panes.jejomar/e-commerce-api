//packages and dependencies
const jwt = require("jsonwebtoken");
const User = require("./models/User");
const secret = "eCommerceAPI";

//function for token creation
module.exports.createAccessToken = (userData) => {

    const data = {
        id: userData._id,
        email: userData.email,
        isAdmin: userData.isAdmin
    };

    return jwt.sign(data, secret, {})
};

module.exports.verify = (req, res, next) => {

	//console.log(req.headers.authorization);
	let token = req.headers.authorization;

	if(typeof token === "undefined"){

		return res.send({auth: "Failed. No Token"});
	
	} else {

		//console.log(token);

		token = token.slice(7, token.length);

		//console.log(token);

		jwt.verify(token, secret, function(err, decodedToken){

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				});

			} else {

				//console.log(decodedToken);
				req.user = decodedToken

				next();
			}
		})
	}
};

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}
