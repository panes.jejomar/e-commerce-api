// [SECTION] Depencies and Modules
let bcrypt = require('bcrypt')
let User = require('./../models/User');
let dotenv = require('dotenv').config();


// [SECTION]
let salt = Number(process.env.SALT);

// [SECTION] [CREATE]
// fn[Sign up] 
module.exports.signUp = (data) =>{
    let userEmail = data.email;
    let userPass = data.password;

    let userRegister = new User({
        email : userEmail,
        password : bcrypt.hashSync(userPass,salt)
    })

    return User.findOne({email: userEmail}).then(foundExistingUser => {
        if (foundExistingUser){
            return ({message:`Email has already been used`})
        }
        else{
            return userRegister.save().then((savedDoc ,saveErr)=>{
                if (saveErr){
                    return ({message:`Error in saving the user`});
                }else{
                    return true;
                }
            })
        }
    })
    
}


// [SECTION] [RETRIEVE]
module.exports.getUserDetails = (userId) =>{
    return User.findById(userId).then(result=>{
        return result;
    }).catch(err =>{return ({message: `${err.message}`})})
}

// [SECTION] [UPDATE]
// fn[isAdmin set to true]
module.exports.setAdmin = async (data) =>{
    let userEmail = data.email;
    
    let isUserAdmin = await User.findOne({email:userEmail}).then(foundUser => {
        if (foundUser.isAdmin === true){
            return true
        }
        return false
    }).catch(err => err.message)
    
    if (isUserAdmin === true){
        return ({message:`${userEmail} is already set as an admin`})
    }


    let isAdminUpdate = {
        isAdmin : true
    }
    
    return User.findOneAndUpdate({email:userEmail} , isAdminUpdate).then(updateSuccess  =>{
       if (updateSuccess){ 
           return ({message:`Success in setting user as an admin`})
          
       }else{
           return ({message:`Error in setting user as an admin`})
       }
    });
    
} 
// fn[isAdmin set false]
module.exports.setAdminAsUser = async (data) =>{
    let userEmail = data.email;

       
    let isAdminUser = await User.findOne({email:userEmail}).then(foundUser => {
        if (foundUser.isAdmin === true){
            return true
        }
        return false
    }).catch(err => err.message)

    if (isAdminUser === false){
        return({message:`${userEmail} is already set as user`}) 
    }


    let isAdminUpdate = {
        isAdmin : false
    }

    return User.findOneAndUpdate({email:userEmail} , isAdminUpdate).then((updateSuccess) =>{
       if (updateSuccess){
            return ({message:`Success in setting admin as a user`})
       }else{
        return ({message:`Error in setting admin as a user`})
       }
    });
    
}




// [SECTION] [DELETE]