// [SECTION] Depencies and Modules
let Product = require("./../models/Product");
let User = require("./../models/User");

// [SECTION] [CREATE]
module.exports.createProduct = (data) => {
  let productName = data.name;
  let productDesc = data.description;
  let productPrice = data.price;

  let newProduct = new Product({
    name: productName,
    description: productDesc,
    price: productPrice,
  });

  return newProduct.save().then((savedProduct, err) => {
    if (err) {
      return { message: `Error in Saving Product` };
    } else {
      return { message: `Success in Saving Product ${savedProduct}` };
    }
  });
};

// [SECTION] [RETRIEVE]
// fn[Get all products]
module.exports.getProducts = () => {
  return Product.find({}).then((foundProducts) => {
    return foundProducts;
  });
};
// fn[Get Single product]
module.exports.getOneProduct = (id) => {
  return Product.findById(id).then((foundProduct) => {
    return foundProduct;
  });
};

// [SECTION] [UPDATE]
// fn[Archiving Products]
module.exports.archiveProduct = async (id) => {
  let isProductActive = await Product.findById(id).then((foundProduct) => {
    if (foundProduct.isActive === true) {
      return true;
    }
    return false;
  });

  if (isProductActive === false) {
    return { message: `${id} is already archived` };
  }

  let productUpdate = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(id, productUpdate)
    .then((updateSuccess) => {
      return true;
    })
    .catch((err) => {
      message: err.message;
    });
};
// fn[Restore Products]
module.exports.restoreProduct = async (id, user) => {
  let isProductActive = await Product.findById(id).then((foundProduct) => {
    if (foundProduct.isActive === true) {
      return true;
    }
    return false;
  });

  if (isProductActive === true) {
    return { message: `${id} is already restored` };
  }

  let productUpdate = {
    isActive: true,
  };

  return Product.findByIdAndUpdate(id, productUpdate)
    .then((updateSuccess) => {
      return true;
    })
    .catch((err) => {
      message: err.message;
    });
};

// fn[updating info in product]
module.exports.updateInformation = (id, data) => {
  let updateProductData = {
    name: data.name,
    description: data.description,
    price: data.price,
  };
  return Product.findByIdAndUpdate(id, updateProductData).then(
    (foundExistingUser) => {
      if (foundExistingUser) {
        return true;
      }
    }
  );
};

module.exports.addCart = async (data, userId) => {
  const isItemAlreadyInCart = await User.findById(userId).then((result) => {
    let count = 0;
    for (let i = 0; result.cart.length > i; i++) {
      if (result.cart[i].ProductID === data) {
        count++;
      }
    }
    if (count > 0) {
      return true;
    } else {
      return false;
    }
  });

  if (isItemAlreadyInCart === true) {
    return { message: "Item Already exists in Cart" };
  }
  const updateUser = {
    cart: [{ ProductID: data }],
  };
  return User.findByIdAndUpdate(userId, { $push: updateUser })
    .then((foundExistingUser) => {
      return true;
    })
    .catch((err) => {
      return { message: err.message };
    });
};

module.exports.getCart = async (userId) => {
  let cartTemp = [];

  const UserCartContent = await User.findById(userId).then(
    (foundExistingUser) => {
      return foundExistingUser.cart;
    }
  );

  if (UserCartContent.length === 0) {
    return false;
  }

  for (let i = 0; UserCartContent.length - 1 >= i; i++) {
    const currentProduct = await Product.findById(UserCartContent[i].ProductID)
      .then((cart) => {
        return cart;
      })
      .catch((err) => {
        return { message: err.message };
      });
    cartTemp.push(currentProduct);
  }

  return cartTemp;
};

module.exports.pullCart = async (data, id) => {
  let temp = [];
  const userCart = await User.findById(id).then((foundUser) => {
    return foundUser.cart;
  });
  for (let i = 0; userCart.length > i; i++) {
    if (userCart[i].ProductID !== data) {
      temp.push({ ProductID: userCart[i].ProductID });
    }
  }

  const updateUser = { cart: temp };
  console.log(updateUser);
  return User.findByIdAndUpdate(id, { $set: updateUser })
    .then((result) => {
     if(result)return true;
      return false
    })
    .catch((err) => {
      message: err.message;
    });
};

// [SECTION] [DELETE]
module.exports.deleteProduct = (id) => {
  return Product.findByIdAndDelete(id)
    .then((result) => {
      return true;
    })
    .catch((err) => {
      message: err.message;
    });
};
