// [SECTION] Depencies and Modules
const express = require("express");
const Order = require("./../models/Order");
const User = require("./../models/User");
const Product = require("./../models/Product");


// [SECTION] [CREATE]

module.exports.orderCreate = async (user, data) => {
  let adminStatus = user.isAdmin;
  let userId = user.id;
  let orderProductId = data.productId;
  let orderProductQuantity = data.productQuantity;
 
  if (adminStatus === true) {
    return ({message:"Action Forbidden"});
  }
  let findProductById = await Product.findById(orderProductId)
    .then((foundProduct) => {
      return foundProduct;
    })
    .catch((err) => err.message);

  let newOrder = new Order({
    totalAmount: (findProductById.price * orderProductQuantity),
    orderBy: userId,
    products: [
      {
        productId: findProductById._id,
        productQuantity: orderProductQuantity,
      },
    ],
  });
 
  let orderCreatedResult = await newOrder
    .save()
    .then((orderCreated) => {
      return orderCreated;
    });
  
  if (orderCreatedResult !== null) {
    
    let updateUser = {
      orders: [
        {
          orderID: orderCreatedResult._id,
        },
      ],
    };

    return User.findByIdAndUpdate(userId,{$push : updateUser})
      .then((foundUser) => {
        
        return (foundUser)
      })
      .catch((err) => err.message);
  }
};

// [SECTION] [RETRIEVE]
// fn[get all users orders]
module.exports.usersOrders = () => {
  return User.find({ orders: { $exists: true, $ne: [] } }).then(
    (foundAllusersWithOrders) => {
      if (foundAllusersWithOrders) {

        return foundAllusersWithOrders;
      } else {
        return ({message:"No User found"});
      }
    }
  );
};


// fn[get current User order]

module.exports.currentUserOrders = async (user) => {
    let adminStatus = await user.isAdmin;
  
  if(adminStatus === true){
    return ({message:'forbidden'})
  }
  return User.findById(user.id).then((foundExistingOrders) => {
    return foundExistingOrders;
  });
};

// fn[get all orders]
module.exports.getallOrders = () => {
  return Order.find({}).then((foundExistingOrders) => {
    return foundExistingOrders;
  });
};
// [SECTION] [UPDATE]
// [SECTION] [DELETE]


