// [SECTION] DEPENDENCIES AND MODULES
let express = require('express');
let controller = require('../controllers/user');
const { verify, verifyAdmin} = require('./../controllers/login');
let login = require('./../controllers/login')

// [SECTION] ROUTING COMPONENT
const route = express.Router();
// [SECTION] [GET]
route.get('/getUserDetails',verify,(req,res)=>{
    let userId = req.user.id;
    controller.getUserDetails(userId).then(result =>{
        res.send(result)
    })

})
// [SECTION] [POST]ROUTES
// fn[REGISTRATION OF NEW USER]
route.post('/register',(req,res) =>{
    let data = req.body;
    controller.signUp(data).then(result =>{
        res.send(result);
    })
});
//trial
route.post('/login', (req, res) => {
    let data = req.body;
    login.loginUser(data).then(result => {
        res.send(result)
    })
})



// [SECTION] [PUT]ROUTES

// fn[SET USER TO ADMIN]
route.put('/setAdmin',verify,verifyAdmin,(req,res) => {
    let data = req.body;
    controller.setAdmin(data).then(result => {
        res.send(result);
    })
});
// fn[SET ADMIN TO USER]
route.put('/setUser',verify,verifyAdmin,(req,res) => {
    let data = req.body;
    controller.setAdminAsUser(data).then(result => {
        res.send(result);
    })
});
// [SECTION] [DEL]ROUTES
// [SECTION] EXPORT ROUTES
module.exports = route


