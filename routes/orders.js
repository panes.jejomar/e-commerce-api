// [SECTION] DEPENDENCIES AND MODULES
let express = require('express');
const { verify,verifyAdmin } = require('./../controllers/login');

let controller = require('./../controllers/order')

// [SECTION] ROUTING COMPONENT

const route = express.Router();
// [SECTION] [GET]ROUTES
route.get('/get-user-orders',verify,(req,res) =>{
    controller.usersOrders().then(result =>{
        res.send(result)
    })
})
route.get('/getAll', verify,verifyAdmin,(req,res)=>{
    controller.getallOrders().then(result =>{
        res.send(result);
    })
})



route.get('/currentOrder',verify,(req,res)=>{
    controller.currentUserOrders(req.user).then(result =>{
        res.send(result)
    })
})
// [SECTION] [PST]ROUTES
route.post('/createOrder',verify,(req,res)=>{
    let data = req.body;
    let user = req.user;
    controller.orderCreate(user,data).then(result =>{
        res.send(result);
    })
    
})


// [SECTION] [PUT]ROUTES
// [SECTION] [DEL]ROUTES
// [SECTION] EXPORT ROUTES
module.exports = route;