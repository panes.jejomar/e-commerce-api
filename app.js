//packages and dependencies
const express = require("express");
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors')


const usersRoutes = require('./routes/user.js');
const productsRoutes = require('./routes/product.js');
//const ordersRoutes = require('./routes/order.js');

//server setup
const app = express();
dotenv.config();
app.use(express.json());
app.use(cors());
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT;

//application routes
app.use('/users', usersRoutes);
app.use('/products', productsRoutes);
//app.use('/orders', ordersRoutes);

//database connection
mongoose.connect(secret);
let dbStatus = mongoose.connection;
dbStatus.on('open', () => {
    console.log(`database is connected`)
});

// gateway response
app.get('/', (req, res) => {
    res.send(`Welcome to JVP online shop!`)
});



app.listen(port, () => {
    console.log(`server is running on ${port}`);
});