// [SECTION] DEPENDENCIES AND MODULES 
const mongoose = require('mongoose');

// [SECTION] BLUEPRINT SCHEMA
let userSchema = new mongoose.Schema({
    email:{
        type: String,
        required :[true,'Email is required']
    },
    password:{
        type:String,
        required : [true, 'Password is required']
    },
    isAdmin:{
        type: Boolean,
        default :false
    },
    cart:[{

        ProductID:{
            type:String,
            required:[true ,'ProductID is required']
        }
    }],
    orders:[{

        orderID:{
            type:String,
            required:[true ,'orderID is required']
        }
    }]
    })
// [SECTION] MODULE EXPORT
let User = mongoose.model('User',userSchema);
module.exports = User;
